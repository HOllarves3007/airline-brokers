<ul class="menu">
  <li><a href="index.php"><span>Home</span></a>
  <li><a href="reservation.php"><span>Reservation</span></a> 
  <li><a href="requestVisa.php"><span>Request a Visa</span></a>
  <li><a href="#" class="parent"><span>Travel</span></a>
    <ul>
      <li><a href="regulations.php">Travel Regulations</a></li>
      <li><a href="groups.php">Licensed Group Travel</a></li>
      <li><a href="family.php">Individual & Family Travel</a></li>
      <li><a href="passportsAndVisas.php">Cuban Passport Renewal & Entry Visas</a></li>
    </ul>
  </li>  
  <li><a href="#" class="parent"><span>Planning</span></a>
    <ul>
      <li><a href="schedule.php">Flight Schedules & Pricing</a></li>
      <li><a href="ticketing.php">Ticketing and Check-in</a></li>
      <li><a href="signatureServices.php">Signature Services</a></li>      
      <li><a href="currency.php">Money & Communications</a>
      <li><a href="exports.php">Exports & Imports</a></li>
      <li><a href="packing.php">What to Pack</a></li>
    </ul>
  </li>
  <li><a href="#"><span>Discover Cuba</span></a>
    <ul>
      <li><a href="groundServices.php">Additional Ground Services</a></li>
      <li><a href="hotels.php">Where to Stay</a></li>
      <li><a href="carRental.php">Car Rental</a>
    </ul>
  </li>
  <li><a href="#"><span>About Us</span></a>
    <ul>
      <li><a href="history.php">History</a></li>
    </ul>
  </li>
  <li><a href="cruises.php"><span>Cruises to Cuba</span></a></li>
  <li class="last"><a href="contact.php"><span>Contact Us</span></a></li>
</ul>
