<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('front/css/app.css') }}" title="" type="" />
        <link rel="stylesheet" href="{{ asset('front/css/site.css') }}" title="" type="" />
        @yield('styles')
        </head>
    <body>
        @include('layouts.front.header')
        @yield('content')
        @include('layouts.front.footer')
        @include('layouts.front.scripts')
        @yield('scripts')
    </body>
</html>



