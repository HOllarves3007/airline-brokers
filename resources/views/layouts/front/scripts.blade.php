<script type="text/javascript" src="{{ asset('front/js/jquery-1.9.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery-browser.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/site.js') }}"></script>
