'use strict';
$(document).ready(function(){
    //jQuery UI
    $(function() {
        $( "#goDate" ).datepicker({ dateFormat: 'mm/dd/yy' }).val();
        $( "#returnDate" ).datepicker({ dateFormat: 'mm/dd/yy' }).val();
        $( "#dateOfTravel" ).datepicker({ dateFormat: 'mm/dd/yy' }).val();
    });

    //jQUery Validation
   $("#validate-reservation").validate({
       ignore: '.ignore',
       submitHandler: function(form){
           $('form input[type=submit]').attr('disabled', 'disabled');
           form.submit();
       },
       rules:{
           'firstName':{
               required: true
           },
           'lastName':{
               required: true
           },
           'email':{
               required: true,
               email: true
           },
           'phone':{
               required: true,
               minlength: 5
           },
           'dob':{
               required: true
           },
           'countryOfBirth':{
               required: true
           },
           'maidenName': {
               required: true
           },
           'address':{
               required: true
           },
           'city':{
               required: true
           },
           'state':{
               required: true
           },
           'zipCode':{
               required: true
           },
           'cubanAddress':{
               required: true
           },
           'cubaCity':{
               required: true
           },
           'terms':{
               required: true
           },
           'purpose':{
               required: true
           },
           'hiddenRecaptcha': {
               required: function () {
                   if (grecaptcha.getResponse() == '') {
                       return true;
                   } else {
                       return false;
                   }
               }
           }
       },
       messages:{

           'firstName': '<span class="invalid"><strong>Required</strong></span>',
           'lastName': '<span class="invalid"><strong>Required</strong></span>',
           'email':{
               required: '<span class="invalid"><strong>Required</strong></span>',
               email: '<span class="invalid"><strong>Invalid</strong></span>'
           },
           'phone':{
               required: '<span class="invalid"><strong>Required</strong></span>',
               minlength: '<span class="invalid"><strong>Too short</strong></span>'
           },
           'dob':'<span class="invalid"><strong>Required</strong></span>',
           'countryOfBirth': '<span class="invalid"><strong>Required</strong></span>',
           'maidenName': '<span class="invalid"><strong>Required</strong></span>',
           'address': '<span class="invalid"><strong>Required</strong></span>',
           'city': '<span class="invalid"><strong>Required</strong></span>',
           'state': '<span class="invalid"><strong>Required</strong></span>',
           'zipCode': '<span class="invalid"><strong>Required</strong></span>',
           'cubanAddress': '<span class="invalid"><strong>Required</strong></span>',
           'cubaCity': '<span class="invalid"><strong>Required</strong></span>',
           'terms': '<span class="invalid"><strong>Required</strong></span>',
           'purpose': '<span class="invalid purposeValidation"><strong>Please choose trip\'s purpose</strong></span>',
           'captcha': '<span class="invalid"><strong>Required</strong></span>',
            }
   });

    $("#validate-contact").validate({
        ignore: '.ignore',
        submitHandler: function(form){
            $('form input[type=submit]').attr('disabled', 'disabled');
            form.submit();
        },
        rules:{
            'name': {
                required:true
            },
            'phone': {
                required:true,
                minlength:5
            },
            'email': {
                required:true,
                email:true
            },
            'message': {
                required:true,
                minlength: 15
            },
            'hiddenRecaptcha': {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            'name': '<span class="invalid"><strong>Required</strong></span>',
            'phone': {
                required: '<span class="invalid"><strong>Required</strong></span>',
                minlength: '<span class="invalid"><strong>Too short</strong></span>'
            },
            'email':{
                required: '<span class="invalid"><strong> Required</strong></span>',
                email: '<span class="invalid"><strong>Invalid</strong></span>'
            },
            'message':{
                required: '<span class="invalid"><strong>Required</strong></span>',
                minlength: '<span class="invalid"><strong>Too short</strong></span>'
            },
            'captcha': '<span class="invalid"><strong>Required</strong></span>'
        }
    });

    $("#validate-visa").validate({
        ignore: '.ignore',
        submitHandler: function(form){
            $('form input[type=submit]').attr('disabled', 'disabled');
            form.submit();
        },
        rules:{
            'fullName': {
                required:true
            },
            'dateOfTravel': {
                required:true
            },
            'airlineConfirmation': {
                required:true
            },
            'nationality': {
                required:true
            },
            'address': {
                required: true
            },
            'address_2': {
                required: true
            },
            'phone': {
                required: true,
                minLength: 5
            },
            'email':{
                required: true,
                email: true
            }
        },
        messages: {
            'fullName': '<span class="invalid"><strong>Required</strong></span>',
            'dateOfTravel': '<span class="invalid"><strong>Required</strong></span>',
            'airlineConfirmation': '<span class="invalid"><strong> Required</strong></span>',
            'nationality': '<span class="invalid"><strong>Required</strong></span>',
            'address': '<span class="invalid"><strong>Required</strong></span>',
            'address_2': '<span class="invalid"><strong>Required</strong></span>',
            'phone':{
                required: '<span class="invalid"><strong>Required</strong></span>',
                minLength: '<span class="invalid"><strong>Too short</strong></span>'
            },
            'email':{
                required:'<span class="invalid"><strong>Required</strong></span>',
                email: '<span class="invalid"><strong>Invalid</strong></span>'
            }
        }
    });
});
